# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from googlesearch import search
from google_trans_new import google_translator
from requests.utils import requote_uri
import requests

class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hoi_dap"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        ketQua = "";
        cauhoi = tracker.latest_message.get('text').lower()
        ngoaichude = True;
        print(cauhoi.find("cho"))
        if cauhoi.find("cho") >= 0 and (cauhoi.find("hỏi") > 1 or  cauhoi.find("hoi") > 1):
            print(cauhoi.find("cho"))
            ngoaichude = False
            
        if cauhoi.find("hỏi") > 1:
            cauhoi = cauhoi[cauhoi.find("hỏi")+3 :]
        elif cauhoi.find("hoi") > 1:
            cauhoi = cauhoi[cauhoi.find("hoi")+3 :]
        else:
            cauhoi = cauhoi;
        print(cauhoi)
        translator = google_translator()  
        translate_text = translator.translate(cauhoi,lang_tgt='en')
        print(translate_text) 
        for url in search(translate_text+ " site:stackoverflow.com", stop=5):
            if(url.find("stackoverflow.com") > 1):
                ketQua = url
                break

        if ngoaichude:
            url = "https://api.simsimi.net/v1/?text="+ requote_uri(tracker.latest_message.get('text').lower()) + "&lang=vi_VN"
            r = requests.get(url)
            dispatcher.utter_message(text=r.json().get("success")) 
        else:
            if(ketQua != ""):
                dispatcher.utter_message(text="Bạn có thể tham khảo câu trả lời từ link "+ url)
            else:
                dispatcher.utter_message(text="Xin lỗi bot không tìm thấy câu trả lời từ stackoverflow bạn vui lòng hỏi lại câu khác!")
        return []
